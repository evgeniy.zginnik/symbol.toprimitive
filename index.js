Object.prototype[Symbol.toPrimitive] = function(hint) {
    console.log(`Transformation - ${hint.toUpperCase()}`);

    const objValueOf = typeof this.valueOf();
    const objToString = typeof this.toString();

    if(!this.toString && !this.valueOf) {
        throw new Error('11111')
    }

    if(hint === 'string') {
        if(objToString !== 'object' && objToString !== 'function') {
            return this.toString();
        };
        if(objValueOf !== 'object' && objValueOf !== 'function') {
            return this.valueOf();
        };
        throw new TypeError();
    } else {
        if(objValueOf !== 'object' && objValueOf !== 'function') {
            return this.valueOf();
        }
        if(objToString !== 'object' && objToString !== 'function') {
            return this.toString();
        }
        throw new TypeError();
    }
}

// Number
console.log(+{});
console.log({} / 2);
console.log([] - 1);

//String
console.log(String(alert));
// console.log(alert({}));
console.log(`${[]}`);

//Default
console.log([1] + {});
